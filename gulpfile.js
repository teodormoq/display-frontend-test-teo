/*global require, console, __dirname */

/* DIS/PLAY gulpfile
 Author's name: Anders Gissel
 Modified by:

 **********************************************************************************************************************
 **********************************************************************************************************************

 Welcome to the Frontline gulpfile. Almost everything you need to get started is in this file, although
 if you are just setting up your project from scratch, you do need to edit a few things:

    1) Start by running this command to get the basic NPM modules installed:
            install.bat
       If you're on a non-Windows platform, ensure you have Bower and the Git-client installed, then run this instead:
            npm install && bower install

    2) Fire up gulp by running "npm run gulp", or check the README.md for information about debugging JS
        and similar. At some point you'll need to edit this file again, but there's plenty of
        comments to start on, so you should be in good hands.

    3) Have fun!


 */


(function () {
	"use strict";

	/**
	 * SETUP
	 */
	var gulp = require('gulp'),


	// Define your destination paths here. You can set as many as you'd like.
		renderDestinationFolders = [
			// '../Website/static',    // <- You can add as many of these output folders as you want.
			// '../Project.Presentation/static',
			// '../Whatever/folder',
			'static/dist'
		],


	// Use these two boolean values to define whether or not to use BrowserSync, LiveReload, or both.
		enableBrowserSync = true,
		enableLiveReload = false,


	// Placeholder for loaded modules, so we won't have to require them more than once. See the function disRequire()
	// further down for more.
		resolvedModules = {},

	// Placeholders for BrowserSync and LiveReload once they're started.
		browsersyncInstance,
		liveReloadInstance;


	/**
	 * Internal function for loading required NPM-modules. The usual methodology is to just load *everything* at the top
	 * of the gulpfile, but as the file grows bigger and includes more and more modules, the initialization time will
	 * get longer and longer and longer.
	 * Utilizing this function will enable us to only load the modules we need for any given task, and to NOT load
	 * heavy image processors if all we really need for the current run is a JSLint.
	 *
	 * @param {string} functionName - must correspond to an NPM module name, such as "gulp-less" or "yargs".
	 * @returns {function}
	 */
	function disRequire(functionName) {
		var returnData;

		// If the function has already been resolved and loaded, just return the cached data now.
		if (resolvedModules[functionName]) {
			returnData = resolvedModules[functionName];
		} else {
			// Utilize NPM to require the requested module
			returnData = require(functionName);
			// Save the object reference for later so we may reuse it.
			resolvedModules[functionName] = returnData;
		}

		// Return the new function.
		return returnData;
	}


	/**
	 * Callback function to let the user know when (and why) stuff is automatically happening while Gulp is running.
	 *
	 * @param {Event} event
	 * @param {string} event.path
	 */
	function watcherCallback(event) {
		var colors = disRequire("colors"),
			msg = 'File ' + event.path + ' was ' + event.type + ', running tasks...',
			msgLength = Math.max(msg.length, 60) + 1;
		console.log(new Array(msgLength).join("=").white.bold);
		console.log(msg.white.bold);
	}


	/**
	 * Output the render pipeline to all known destination paths.
	 *
	 * @param {object} renderPipeline - The render pipeline
	 * @param {string} [additionalPath=""] Additional path to append to the output destination
	 * @returns {object}
	 */
	function renderToDestinations(renderPipeline, additionalPath) {
		var currentFolderIndex;

		for (currentFolderIndex = 0; currentFolderIndex < renderDestinationFolders.length; currentFolderIndex++) {
			renderPipeline = renderPipeline.pipe(gulp.dest(renderDestinationFolders[currentFolderIndex] + (additionalPath || "")));
		}

		return renderPipeline;
	}


	/**
	 * Set up all the watchers needed.
	 */
	function setupWatchers() {
		if (enableBrowserSync) {
			browsersyncInstance = disRequire('browser-sync').create();
			var browserSyncSSI = disRequire('browsersync-ssi');

			browsersyncInstance.init({
				server: {
					baseDir: "./",
					middleware: browserSyncSSI({
						baseDir: __dirname,
						ext: '.shtml'
					})
				}
			});


			// Watch the 'module-demohtml' folder and the root folder for html and shtml files and refresh the browser when changes happen.
			gulp
				.watch(['module-demohtml/**/*.html', 'module-demohtml/**/*.shtml', '*.html', '*.shtml'])
				.on('change', browsersyncInstance.reload);
		}


		if (enableLiveReload) {
			liveReloadInstance = disRequire("gulp-livereload");

			// Set up the livereload-server.
			liveReloadInstance.listen();

			// Watch the entire DIST folder and refresh the browser when changes happen.
			gulp
				.watch(['static/dist/**'])
				.on('change', liveReloadInstance.changed);
		}


		// Fire the resourcecopy-task when any of our source files (images, fonts, whatever) are added or changed.
		gulp
			.watch(['static/src/fonts/**', 'static/src/img/**', 'static/src/media/**/*.*', 'static/src/js/vendor/modernizr*'], ['resourcecopy'])
			.on('change', watcherCallback);

		// Compile svg-resources when new icons are added, or just... whenever.
		gulp
			.watch(['static/src/svg-icons/**/*.*'], ['svgstore'])
			.on('change', watcherCallback);

		// Watch JS-folder(s) for changes, and compile/lint as necessary.
		gulp
			.watch('static/src/js/**/*.js', ['js', 'lint'])
			.on('change', watcherCallback);

		// Watch LESS- or SASS-folder for changes, and compile as necessary.
		gulp
			.watch('static/src/sass/**/*.*', ['sass'])
			.on('change', watcherCallback);
	}


	/**
	 * DEFAULT TASK
	 *
	 * This function runs unless you specifically specify a task (by running "gulp resourcecopy",
	 * "gulp less", or similar). It performs the usual subtasks (copying resources, compiling
	 * CSS and JS), and then sets up file watchers to handle livereload and compile-on-change
	 * actions.
	 */
	gulp
		.task('default',
			// These are the tasks that are run in succession when gulp is first started.
			[
				'svgstore',
				'resourcecopy',
				'sass',
				'js',
				'lint'
			],
			// Once all the tasks have finished, this callback will fire to set up the watchers and live-reload.
			setupWatchers
		);


	/**
	 * WATCHER
	 *
	 * This function is more or less the same as the default, except it doesn't run the compilation
	 * scripts at first run, and instead just sets up the watchers right away.
	 */
	gulp
		.task('watch',
			// These are the tasks that are run in succession when gulp is first started.
			[],
			// Once all the tasks have finished, this callback will fire to set up the watchers and live-reload.
			setupWatchers
		);


	/**
	 * JAVASCRIPT
	 *
	 * Set up a task for compiling javascript. This will both compile the main javascript file,
	 * containing all your various vendor modules as well as your own code, and the legacy
	 * script file needed for IE8 (and below) for understanding modern web code.
	 *
	 * DO NOT include Modernizr or jQuery here. The former should be included explicitly in the
	 * page headers, along with legacy.min.js, the latter should ideally be included in the page
	 * footer along with your main JS file.
	 */
	gulp
		.task('js', function () {

			// Set up variables for the NPM modules we'll need, and load those we're sure to run.
			var sourceFiles,
				uglify,
				babel = disRequire("gulp-babel"),
				concat = disRequire("gulp-concat"),
				sourcemaps = disRequire('gulp-sourcemaps'),
				cmdArguments = disRequire('yargs').argv,
				colors = disRequire("colors"),
				plumber = disRequire("gulp-plumber"),
				renderPipeline,
				legacyRenderPipeline,
				babelConfig = {
					presets: ['es2015'],
					only: ['dis.*.js'] // Only transpile our own scripts using Babel
				};

			if (cmdArguments.debug) {
				console.log(" ");
				console.log("   ************************************************".yellow);
				console.log("   * SCRIPT IS BEING COMPILED IN DEBUG-MODE!      *".yellow);
				console.log("   * No minification will be performed.           *".yellow);
				console.log("   ************************************************".yellow);
				console.log(" ");
			} else {
				// Get the uglify-module ready
				uglify = disRequire("gulp-uglify");
			}


			sourceFiles = [
				// 'static/src/js/vendor/some.jquery.plugin.js',
				// 'static/src/js/vendor/some.other.jquery.plugin.js',
				// 'static/src/js/vendor/jquery.*.js', // Include all jQuery-plugin files, usually named jQuery.pluginname.js, unconditionally. Use with caution.
				'bower_components/imagesloaded/imagesloaded.pkgd.js',
				'bower_components/svg4everybody/dist/svg4everybody.js',
				'bower_components/underscore/underscore.js',
				'static/src/js/dis.base.js',
				'static/src/js/dis.base.*.js',
				'static/src/js/dis.module.*.js',
				'static/src/js/**/dis.module.*.js',
				'static/src/js/dis.main.js'
			];

			renderPipeline = gulp
				.src(sourceFiles)
				.pipe(plumber())
				.pipe(sourcemaps.init());

			if (!cmdArguments.debug) {
				renderPipeline = renderPipeline
					.pipe(babel(babelConfig))
					.pipe(concat("main.min.js"))
					.pipe(uglify({}));
			} else {
				renderPipeline = renderPipeline
					.pipe(babel(babelConfig))
					.pipe(concat("main.min.js"));
			}


			// Write the source maps
			renderPipeline = renderPipeline.pipe(sourcemaps.write('.'));


			// Stop the plumber.
			renderPipeline.pipe(plumber.stop());

			// Render the output into the DIST folder(s)
			renderPipeline = renderToDestinations(renderPipeline, "/js");

			if (browsersyncInstance) {
				renderPipeline.pipe(browsersyncInstance.stream());
			}


			// Create a file for legacy-browsers, such as IE8 and below, to get rudimentary support for HTML5 and similar.
			legacyRenderPipeline = gulp.src([
					'static/src/js/vendor/consolelogfix.min.js',
					'static/src/js/vendor/html5shiv-printshiv.js',
					'static/src/js/vendor/respond.min.js'
				])
				.pipe(plumber())
				// Uglify the javascript files into "legacy.min.js"
				.pipe(concat('legacy.min.js'))
				.pipe(plumber.stop());

			// Render the output into the DIST folder(s)
			renderToDestinations(legacyRenderPipeline, "/js");
		});


	/**
	 * JS Linter
	 *
	 * Set up task for JSHint.
	 */
	gulp
		.task("lint", function () {
			var jshint = disRequire("gulp-jshint");

			gulp.src("static/src/js/*.js")
				// Run file through JSHint
				.pipe(jshint())
				// Pipe the result of that through to the JSHint reporter
				.pipe(jshint.reporter("default"));
		});


	/**
	 * SASS
	 *
	 * Set up task for SASS
	 */
	gulp
		.task('sass', function () {

			// Get all the NPM-modules ready.
			var sass = disRequire("gulp-sass"),
				sassGlob = disRequire('gulp-sass-glob'),
				sourcemaps = disRequire('gulp-sourcemaps'),
				cssPrefixer = disRequire("gulp-autoprefixer"),
				plumber = disRequire("gulp-plumber"),
				rename = disRequire("gulp-rename"),
				cssNano = disRequire("gulp-cssnano"),
				renderPipeline;

			renderPipeline = gulp.src('static/src/sass/main.scss')
				.pipe(plumber())
				.pipe(sourcemaps.init())
				// Run file through SASS-glob and -compiler
				.pipe(sassGlob())
				.pipe(sass())
				// Auto-prefix CSS3-properties
				.pipe(cssPrefixer())
				// Minify the CSS
				.pipe(cssNano())
				// Rename CSS file
				.pipe(rename({suffix: '.min', extname: '.css'}))
				.pipe(sourcemaps.write('.'))
				.pipe(plumber.stop());

			// Render the output into the DIST folder(s)
			renderPipeline = renderToDestinations(renderPipeline, "/css");


			if (browsersyncInstance) {
				renderPipeline.pipe(browsersyncInstance.stream({match: '**/*.css'}));
			}
		});


	/**
	 * Create SVG-sprite file from SVG-sources
	 */
	gulp
		.task('svgstore', function () {

			var svgmin = disRequire("gulp-svgmin"),
				path = disRequire("path"),
				rename = disRequire("gulp-rename"),
				plumber = disRequire("gulp-plumber"),
				svgstore = disRequire("gulp-svgstore"),
				renderPipeline;

			renderPipeline = gulp
				.src('static/src/svg-icons/**/*.svg')
				.pipe(plumber())
				.pipe(svgmin(function (file) {
					var prefix = path.basename(file.relative, path.extname(file.relative));
					return {
						plugins: [{
							cleanupIDs: {
								prefix: prefix + '-',
								minify: true
							}
						}]
					};
				}))
				.pipe(svgstore())
				.pipe(rename("icons.svg"))
				.pipe(plumber.stop());


			renderPipeline = renderToDestinations(renderPipeline);

			if (browsersyncInstance) {
				renderPipeline.pipe(browsersyncInstance.stream({match: '**/*.svg'}));
			}
		});


	/**
	 * RESOURCECOPY
	 *
	 * Set up tasks for copying folders and files to /dist/
	 * and compress images.
	 */
	gulp
		.task('resourcecopy', function () {

			var jQueryLegacyPipeline,
				jQueryModernPipeline,
				renderPipeline;

			// Copy legacy jQuery to the vendor directory
			jQueryLegacyPipeline = gulp.src(['bower_components/jquery-legacy/dist/jquery.min.*']);
			jQueryLegacyPipeline = renderToDestinations(jQueryLegacyPipeline, '/js/vendor/jquery-legacy');

			// Copy modern jQuery to the vendor directory as well.
			jQueryModernPipeline = gulp.src(['bower_components/jquery-modern/dist/jquery.min.*']);
			jQueryModernPipeline = renderToDestinations(jQueryModernPipeline, '/js/vendor/jquery-modern');


			renderPipeline = gulp.src([
				'static/src/fonts/**/*.*',
				'static/src/img/**/*.*',
				'static/src/media/**/*.*',
				'static/src/js/vendor/modernizr*'
			], {base: 'static/src'});

			// Render the output into the DIST folder(s)
			renderPipeline = renderToDestinations(renderPipeline);

			if (browsersyncInstance) {
				renderPipeline.pipe(browsersyncInstance.stream());
			}

		});


}());
