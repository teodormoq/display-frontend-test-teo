/*jshint esnext: true */
/*global jQuery, dis */

/* DIS/PLAY Script
 Author's name: ...
 Modified by:
 Client name: ...
 Date of creation: ...
 */

(function ($, dis) {
	"use strict";


	function init() {

		// Let's say we want to instantiate our ExampleModule (see dis.module.example.js) on all occurrences
		// of the "article" element. We can do this like so:
		$("article").each(function () {

			// Instantiate the new module. It will set itself up and run all initialization scripts, so at this point
			// you shouldn't HAVE to do more, depending on your code and how complex your module is.
			var exampleArticle = new dis.ExampleModule({container: this});

			// However, you can, of course, run a public function on the new module at once:
			exampleArticle.someFunction();

		});

		window.console.log("Frontline is ready!");


		// DIS/PLAY frontend test specific
		var $information = $(".test-information"),
			$infoBtn = $information.find("button");

		$infoBtn.on("click", function() {
			if ($information.hasClass("is-hidden")) {
				$information.removeClass("is-hidden");
			} else {
				$information.addClass("is-hidden");
			}
		});
	}


	// Initialize the main script(s) once the DOM is ready. Not only does this mean that all your DOM-references will
	// work, but more importantly: Gulp is compiling all your scripts into one big file, but the closures are run as soon
	// as they're available. Meaning: if your "main" closure is run BEFORE your "module"-closure, it might try to instantiate
	// your modules before the browser actually knows them. Using jQuery's ready-function alleviates this problem.
	dis.BaseModule.prototype.onReady(init);


}(jQuery, dis));

