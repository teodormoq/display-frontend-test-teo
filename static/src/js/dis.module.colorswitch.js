"use strict";

$(".module-colorswitch--menu a").on("click", function() {

	var string = $(this).text();
	var closest = $(this.closest(".module-colorswitch"));
	if (~string.indexOf("red")) {
		closest.removeClass("module-colorswitch--blue");
		closest.toggleClass("module-colorswitch--red");
	} else {
		closest.removeClass("module-colorswitch--red");
		closest.toggleClass("module-colorswitch--blue");
	}
});

$(".module-colorswitch h2").on("click", function() {
	$(this).parent().find("nav").toggle("slow");
	$(this).parent().toggleClass('closed');
});

$(document).on('keypress', function(e) {
    if (e.which === 49) {
    	$(".module-colorswitch#first").find("nav").toggle("slow");
		$(".module-colorswitch#first").toggleClass('closed');
    } else if (e.which === 50) {
		$(".module-colorswitch#middle").find("nav").toggle("slow");
		$(".module-colorswitch#middle").toggleClass('closed');
    } else if (e.which === 51){
    	$(".module-colorswitch#last").find("nav").toggle("slow");
    	$(".module-colorswitch#last").toggleClass('closed');
    }
});