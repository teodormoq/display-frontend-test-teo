# DIS/PLAY FRONTEND TEST

This is a short test and presentation af the DIS/PLAY Frontline boilerplate.



## Contributors

Team members: Anders Gissel, Bjarni Olsen, bjo@dis-play.dk

Date of creation: 2016

Modified by: Bjarni Olsen, bjo@dis-play.dk

Modified date: 24-08-2016



## FRONTEND DOCUMENTATION

All frontend files are located in `/static` folder.
Frontend resources are compiled with the NodeJS-module Gulp, the configuration for which is located in `gulpfile.js` and `package.json`.

### Install frontend build tools


#### Install NodeJS
To compile the source files for production you need to use Gulp, and in order to use Gulp, you need NodeJS.
If you've already installed it on your system, skip this bit. Otherwise, get NodeJS here:

[Install Node.js](http://nodejs.org/download/ "Node JS website")

... and install it.


#### Bower git-scm
In order to use Bower, you need `git-scm` on your system. If you haven't installed it yet, get it from https://git-scm.com/downloads - when installing,
make sure to select the option that installs Git for use in your regular command line. You can leave all other options at their default setting.


#### Install site-specific Gulp modules
This installs Gulp and all Gulp modules used for this site.

```
c:\path\to\project\root\folder> install.bat
```

Wait for the NodeJS package manager (npm) to install all required Gulp modules and Bower resources. You need only do this once, or if the package information (found inside package.json) changes.
If you're on a Mac, you'll have to run `npm install` and `bower install` manually.

Please consider using Bower for "generic" resources such as jQuery, Bootstrap and the likes.



### Run Gulp
Run this command to start Gulp:

```
c:\path\to\project\root\folder\ > npm run gulp
```

Gulp 'watches' file changes and builds CSS and JS from the source files automatically.


### Configurable output folders
Near the top of `gulpfile.js` you'll find an array called `renderDestinationFolders`. By default there's only one entry in there (`static/dist`), but you
can add as many as you want or need. To output into a "Website"-folder one level above your working folder, you'll want to add `../Website/static`.



### Stylesheets

SASS is located in `/static/src/sass/` and Gulp builds it to `(output-folder-here)/css/`. CSS is minified and sourcemap-enabled.

> Remember to describe any SASS specific structures and wizardry that is not obvious.


### Javascript
(jQuery / Other).

All source code is placed inside `/static/src/js/`, and compiled into `(output-folder-here)/js/`, which is then the only
folder needed on the webserver. All JS is uglified, "mangled" and sourcemap-enabled by default.

External plugins that are not available through Bower must be located in `static/src/js/vendor/plugins`. As a rule, you should only include uncompressed files
(including headers, for versions and copyrights), and let Gulp minify them by including them in your workflow.



### Debugging in Internet Explorer

```
c:\path\to\project\root\folder> npm run gulpdebug
```

or

```
c:\path\to\project\root\folder> gulp js --debug
```

This builds the JS _without_ using minification. This lets you debug the code in Internet Explorer more easily.
Please don't check in the un-minified file, though, or you will be forced to buy everyone cake. And beer.


### Running "watch" task
Instead of running `npm run gulp`, use `npm run gulpwatch`. This will set up the file watchers, but not perform the
initial "compile" of your files. This allows for a faster start.



### Livereload / BrowserSync.
The primary setup contains both LiveReload and BrowserSync. You can use either, both, or neither as you desire.

#### BrowserSync
BrowserSync is enabled by default, and has support for SHTML-files. If you don't want or need BrowserSync, edit `gulpfile.js` and set `enableBrowserSync` to `false`.


#### LiveReload
Livereload is deactivated by default. If you need it, edit `gulpfile.js` and set `enableLiveReload` to `true`. This enables you to make frontend changes and see them live without reloading the entire page.

To use it you need to install this Chrome extension:

```
http://bit.ly/IKI2MY
```

Or the same for Firefox:

```
http://mzl.la/1uAab9V
```

Or if you need to Livereload another browser (eg. on a mobile phone/tablet) then place this script before `</body>` tag:

`<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>`

> Remember to remove it before you deploy to production!
